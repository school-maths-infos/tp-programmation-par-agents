package jc.info925.tp1;

import jc.info925.tp1.model.*;
import jc.info925.tp1.view.Main;

import java.util.ArrayList;
import java.util.Collections;

public class SimulationMotor {

    private Environment environment;
    private ArrayList<Agent> agents;

    private static SimulationMotor simulationMotor;

    public static SimulationMotor getInstance() {
        if(simulationMotor == null) {
            simulationMotor = new SimulationMotor();
        }
        return simulationMotor;
    }

    public static void main(String[] args) {
        SimulationMotor motor = getInstance();
        motor.run();

    }

    private void run() {

        this.generateEnvironment();
        this.placeAgents();

        Main main = new Main(this.environment);

        boolean terminate = false;
        while(!terminate) {
            // Pheromones evaporation
            this.environment.evaporation();

            for(Agent agent : agents) {
                agent.next();
            }
            main.updateFrame();
            try {
                System.out.println("");
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void placeAgents() {

        this.agents = new ArrayList<>();

        for (int k = 0; k < Constant.NB_EXCAVATOR; k++) {
            this.agents.add(new Escavator());
        }
        for (int k = 0; k < Constant.NB_EXPLORATOR; k++) {
            this.agents.add(new Explorator());
        }
        for (int k = 0; k < Constant.NB_TRANSPORTER; k++) {
            this.agents.add(new Transporter());
        }
        Collections.shuffle(this.agents);


        int k = 0;
        boolean stationPlaced = false;
        int nbAgents = Constant.NB_EXCAVATOR + Constant.NB_EXPLORATOR + Constant.NB_TRANSPORTER;
        Position posStation = new Position(0,0);
        for (int i = 0; i < Constant.HEIGHT && k < nbAgents; i++) {
            for (int j = 0; j < Constant.WIDTH && k < nbAgents; j++) {
                if (this.environment.isEmpty(i,j)) {
                    if(!stationPlaced) {
                        posStation = new Position(i,j);
                        Station station;
                        station = new Station();
                        this.environment.setContent(i,j,station);
                        stationPlaced = true;
                    } else {
                        Agent agent = this.agents.get(k);
                        agent.init(this.environment, i, j, posStation.getI(), posStation.getJ());
                        Empty empty = new Empty();
                        this.environment.setContent(i, j, new Empty());
                        empty.setAgent(agent);
                        k++;
                    }
                }
            }
        }
    }

    private void generateEnvironment() {
        this.environment = new Environment();
    }
}
