package jc.info925.tp1.model;

import jc.info925.tp1.Constant;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by celinederoland on 2/3/16.
 */
public class Transporter extends Agent {

    private int move = 0;
    private boolean hasOre = false;

    public Transporter() {
        super();
    }

    public Color getColor() {
        return Color.CYAN;
    }

    @Override
    public void next() {

        if (move == 0) {

            ArrayList<Position> caseEmpties = new ArrayList<>();
            ArrayList<Position> caseHeap = new ArrayList<>();
            Position caseStation = null;

            int minI = Math.max(this.i - 1, 0);
            int minJ = Math.max(this.j - 1, 0);
            int maxI = Math.min(this.i + 1, Constant.HEIGHT - 1);
            int maxJ = Math.min(this.j + 1, Constant.WIDTH - 1);

            for (int line = minI; line <= maxI; line++) {
                for (int column = minJ; column <= maxJ; column++) {
                    if (line != this.i || column != this.j) {
                        if (this.environment.getContent(line, column).isEmpty()) {
                            caseEmpties.add(new Position(line, column));
                        } else if (this.environment.getContent(line, column).isHeap()) {
                            caseHeap.add(new Position(line, column));
                        } else if (this.environment.getContent(line, column).isStation()) {
                            caseStation = new Position(line, column);
                        }
                    }
                }
            }

            // Put his ore to the station
            if (caseStation != null && this.hasOre) {
                this.hasOre = false;
            }
            // Get heap
            else if (!caseHeap.isEmpty() && !this.hasOre) {
                int index = (int) Math.floor(Math.random() * caseHeap.size());
                this.environment.setContent(caseHeap.get(index).getI(), caseHeap.get(index).getJ(), new Empty());
                this.hasOre = true;
            }
            // Search ore following pheromones
            else if (!this.hasOre && !caseEmpties.isEmpty()) {
                this.chooseCaseEmpty(caseEmpties);
            }
            // Go to the station with his ore
            else if (this.hasOre && !caseEmpties.isEmpty()) {
                this.moovToEmptyInStationDirection(caseEmpties);
            }
        }

        move = (move + 1) % 3;
    }

    private void chooseCaseEmpty(ArrayList<Position> caseEmpties) {
        // Initialization max pheromones
        Empty empty = (Empty) this.environment.getContent(caseEmpties.get(0).getI(), caseEmpties.get(0).getJ());
        int maxPheromone = empty.getNbPheromonesEscavator();
        int index = 0;

        for(int i = 1; i < caseEmpties.size(); i++) {
            empty = (Empty) this.environment.getContent(caseEmpties.get(i).getI(), caseEmpties.get(i).getJ());
            if(empty.getNbPheromonesEscavator() > maxPheromone) {
                index = i;
                maxPheromone = empty.getNbPheromonesEscavator();
            }
        }

        if(maxPheromone > 0) {
            this.moovToIndex(caseEmpties, index);
        } else {
            this.moovToEmpty(caseEmpties);
        }
    }
}
