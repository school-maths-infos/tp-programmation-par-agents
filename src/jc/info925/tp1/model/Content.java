package jc.info925.tp1.model;

import java.awt.*;

/**
 * Created by celinederoland on 2/3/16.
 */
public abstract class Content {

    public boolean isEmpty() {
        return false;
    }

    public abstract Color getColor();

    public boolean isOre() {
        return false;
    }

    public boolean isHeap() { return false; }

    public boolean isStation() { return false; }
}
