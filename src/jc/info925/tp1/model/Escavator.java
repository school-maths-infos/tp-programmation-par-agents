package jc.info925.tp1.model;

import jc.info925.tp1.Constant;

import java.awt.*;
import java.util.ArrayList;

public class Escavator extends Agent {

    private int isPheromoneSince = Constant.NB_TURNS_PHEROMONES_ESCAVATOR;
    private int move = 0;

    public Escavator() {
        super();
    }

    public Color getColor() {
        return Color.RED;
    }

    @Override
    public void next() {
        if(move == 0) {
            ArrayList<Position> caseEmpties = new ArrayList<>();
            ArrayList<Position> caseOres = new ArrayList<>();
            ArrayList<Position> caseStation = new ArrayList<>();

            // Put pheromones on his way
            this.putPheromones();

            // Look cases to move
            this.searchCasesAround(caseEmpties, caseOres, caseStation);

            if(!caseStation.isEmpty()) {
                this.stopPheromones();
            }
            if (!caseOres.isEmpty()) {
                int index = (int) Math.floor(Math.random() * caseOres.size());
                this.environment.setContent(caseOres.get(index).getI(), caseOres.get(index).getJ(), new Heap());
                this.isPheromoneSince = 0;
            } else if (!caseEmpties.isEmpty()) {
                this.chooseCaseEmpty(caseEmpties);
            }
        }

        move = (move + 1) % 2;
    }

    private void stopPheromones() {
        this.isPheromoneSince = Constant.NB_TURNS_PHEROMONES_ESCAVATOR;
    }

    private void chooseCaseEmpty(ArrayList<Position> caseEmpties) {
        // Initialization max pheromones
        Empty empty = (Empty) this.environment.getContent(caseEmpties.get(0).getI(), caseEmpties.get(0).getJ());
        int maxPheromone = empty.getNbPheromonesExplorator();
        int index = 0;

        for(int i = 1; i < caseEmpties.size(); i++) {
            empty = (Empty) this.environment.getContent(caseEmpties.get(i).getI(), caseEmpties.get(i).getJ());
            if(empty.getNbPheromonesExplorator() > maxPheromone) {
                index = i;
                maxPheromone = empty.getNbPheromonesExplorator();
            }
        }

        if(maxPheromone > 0) {
            this.moovToIndex(caseEmpties, index);
        } else {
            this.moovToEmpty(caseEmpties);
        }
    }

    private void putPheromones() {
        if (this.isPheromoneSince < Constant.NB_TURNS_PHEROMONES_ESCAVATOR) {
            Empty e = (Empty) this.environment.getContent(this.i,this.j);
            e.increaseNbPheromonesEscavator(Constant.NB_PHEROMONE_PER_TURN_ESCAVATOR);
            this.isPheromoneSince++;
        }
    }
}
