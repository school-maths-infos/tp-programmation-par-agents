package jc.info925.tp1.model;

import jc.info925.tp1.Constant;

import java.util.ArrayList;

/**
 * Created by celinederoland on 2/3/16.
 */
public abstract class Agent extends Content {

    protected int i;
    protected int j;
    protected Environment environment;
    protected int posStationI;
    protected int posStationJ;

    public abstract void next();

    public void init(Environment environment, int i, int j, int posStationI, int posStationJ) {
        this.environment = environment;
        this.i = i;
        this.j = j;
        this.posStationI = posStationI;
        this.posStationJ = posStationJ;
    }

    protected void moovToEmpty(ArrayList<Position> caseEmpties) {
        int index = (int) Math.floor(Math.random() * caseEmpties.size());
        this.moovToIndex(caseEmpties, index);
    }

    protected void moovToIndex(ArrayList<Position> caseEmpties, int index) {
        Empty e = (Empty) this.environment.getContent(this.i, this.j);
        e.setAgent(null);
        this.i = caseEmpties.get(index).getI();
        this.j = caseEmpties.get(index).getJ();
        e = (Empty) this.environment.getContent(caseEmpties.get(index).getI(), caseEmpties.get(index).getJ());
        e.setAgent(this);
    }

    protected void moovToEmptyInStationDirection(ArrayList<Position> caseEmpties) {
        int chosenIndex = 0;
        int squarredDist = Integer.MAX_VALUE;
        for (int index = 0; index < caseEmpties.size(); index++) {
            Position caseEmpty = caseEmpties.get(index);
            int squarredDistCurrent = (caseEmpty.getI() - this.posStationI)*(caseEmpty.getI() - this.posStationI) + (caseEmpty.getJ() - this.posStationJ)*(caseEmpty.getJ() - this.posStationJ);
            if (squarredDistCurrent < squarredDist) {
                chosenIndex = index;
                squarredDist = squarredDistCurrent;
            }
        }
        this.moovToIndex(caseEmpties,chosenIndex);
    }

    protected void searchCasesAround(ArrayList<Position> caseEmpties, ArrayList<Position> caseOres, ArrayList<Position> caseStation) {

        int minI = Math.max(this.i - 1, 0);
        int minJ = Math.max(this.j - 1, 0);
        int maxI = Math.min(this.i + 1, Constant.HEIGHT - 1);
        int maxJ = Math.min(this.j + 1, Constant.WIDTH - 1);

        for (int line = minI; line <= maxI; line++) {
            for(int column = minJ; column <= maxJ; column++) {
                if(line != this.i || column != this.j) {
                    if(this.environment.getContent(line, column).isEmpty()) {
                        caseEmpties.add(new Position(line, column));
                    } else if(this.environment.getContent(line, column).isOre()) {
                        caseOres.add(new Position(line, column));
                    } else if (this.environment.getContent(line,column).isStation()) {
                        caseStation.add(new Position(line, column));
                    }
                }
            }
        }

    }
}
