package jc.info925.tp1.model;

import java.awt.*;

/**
 * Created by celinederoland on 2/3/16.
 */
public class Empty extends Content {
    private Agent agent;
    private int nbPheromonesExplorator;
    private int nbPheromonesEscavator;

    public boolean isEmpty() {
        return (this.getAgent() == null);
    }

    public Color getColor() {
        if (this.getAgent() == null) {
            return Color.WHITE;
        } else {
            return this.getAgent().getColor();
        }
    }

    public int getNbPheromonesExplorator() {
        return nbPheromonesExplorator;
    }

    public void increaseNbPheromonesExplorator(int nbPheromonesExplorator) {
        this.nbPheromonesExplorator += nbPheromonesExplorator;
    }

    public void evaporatePheromonesExplorator() {
        if (this.nbPheromonesExplorator > 0) {
            this.nbPheromonesExplorator--;
        }
    }

    public int getNbPheromonesEscavator() {
        return nbPheromonesEscavator;
    }

    public void increaseNbPheromonesEscavator(int nbPheromonesEscavator) {
        this.nbPheromonesEscavator += nbPheromonesEscavator;
    }

    public void evaporatePheromonesEscavator() {
        if (this.nbPheromonesEscavator > 0) {
            this.nbPheromonesEscavator--;
        }
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
}
