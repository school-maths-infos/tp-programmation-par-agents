package jc.info925.tp1.model;

import java.awt.*;

/**
 * Created by celinederoland on 2/3/16.
 */
public class Station extends Content {
    @Override
    public Color getColor() {
        return Color.GREEN;
    }

    public boolean isStation() { return true; }
}
