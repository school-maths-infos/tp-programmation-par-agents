package jc.info925.tp1.model;

import jc.info925.tp1.Constant;

/**
 * Created by celinederoland on 2/3/16.
 */
public class Environment {

    private Content[][] matrice = new Content[Constant.HEIGHT][Constant.WIDTH];

    public Environment() {
        for(int i = 0; i < Constant.HEIGHT; i++) {
            for(int j = 0; j < Constant.WIDTH; j++) {
                double random = Math.random();
                if(random < Constant.PROBA_ORE) {
                    this.matrice[i][j] = new Ore();
                } else if(random < Constant.PROBA_ORE + Constant.PROBA_OBSTACLE) {
                    this.matrice[i][j] = new Obstacle();
                } else {
                    this.matrice[i][j] = new Empty();
                }
            }
        }
    }

    /**
     * Method to evaporate pheromones
     * on each empty case
     */
    public void evaporation() {
        for(int i = 0; i < Constant.HEIGHT; i++) {
            for(int j = 0; j < Constant.WIDTH; j++) {
                if(this.matrice[i][j].isEmpty()) {
                    Empty empty = (Empty) this.matrice[i][j];
                    empty.evaporatePheromonesEscavator();
                    empty.evaporatePheromonesExplorator();
                }
            }
        }
    }

    public boolean isEmpty(int i, int j) {
        return this.matrice[i][j].isEmpty();
    }

    public void setContent(int i, int j, Content content) {
        this.matrice[i][j] = content;
    }

    public Content getContent(int i, int j) {
        return this.matrice[i][j];
    }
}
