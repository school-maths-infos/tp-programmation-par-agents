package jc.info925.tp1.model;

import jc.info925.tp1.Constant;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by celinederoland on 2/3/16.
 */
public class Explorator extends Agent {

    private int isPheromoneSince = Constant.NB_TURNS_PHEROMONES_EXPLORATOR;
    public Explorator() {
        super();
    }

    public Color getColor() {
        return Color.BLUE;
    }

    @Override
    public void next() {

        this.putPheromones();

        ArrayList<Position> caseEmpties = new ArrayList<>();
        ArrayList<Position> caseOres = new ArrayList<>();
        ArrayList<Position> caseStation = new ArrayList<>();

        this.searchCasesAround(caseEmpties, caseOres, caseStation);

        if(!caseStation.isEmpty()) {
            this.stopPheromones();
        }
        if(!caseOres.isEmpty()) {
            this.startPheromones();
        }
        if(!caseEmpties.isEmpty() && this.isPheromoneSince >= Constant.NB_TURNS_PHEROMONES_EXPLORATOR) {
            this.moovToEmpty(caseEmpties);
        } else if (!caseEmpties.isEmpty() && this.isPheromoneSince < Constant.NB_TURNS_PHEROMONES_EXPLORATOR) {
            moovToEmptyInStationDirection(caseEmpties);
            this.isPheromoneSince++;
        }
    }

    protected void moovToEmpty(ArrayList<Position> caseEmpties) {

        ArrayList<Integer> probaCum = new ArrayList<>();
        int probaTotal = 0;
        for (Position caseEmpty : caseEmpties) {
            int squarredDistCurrent = (caseEmpty.getI() - this.posStationI)*(caseEmpty.getI() - this.posStationI) + (caseEmpty.getJ() - this.posStationJ)*(caseEmpty.getJ() - this.posStationJ);
            probaTotal += squarredDistCurrent;
            probaCum.add(probaTotal);
            System.out.println("caseEmpty = " + caseEmpty.getI() + " - " + caseEmpty.getJ());
            System.out.println("proba = " + squarredDistCurrent);

        }

        int alea = (int) Math.floor(Math.random() * probaTotal);
        int chosenIndex = 0;
        for (int index = 0; index < probaCum.size() - 1; index++) {
            if (alea > probaCum.get(index) && alea < probaCum.get(index+1) ) {
                chosenIndex = index;
                break;
            }
        }
        if (alea > probaCum.get(probaCum.size() - 1)) {
            chosenIndex = probaCum.size() - 1;
        }

        this.moovToIndex(caseEmpties,chosenIndex);
    }

    private void stopPheromones() {
        this.isPheromoneSince = Constant.NB_TURNS_PHEROMONES_EXPLORATOR;
    }


    private void putPheromones() {
        if (this.isPheromoneSince < Constant.NB_TURNS_PHEROMONES_EXPLORATOR) {
            Empty e = (Empty) this.environment.getContent(this.i,this.j);
            e.increaseNbPheromonesExplorator(Constant.NB_PHEROMONE_PER_TURN_EXPLORATOR);
        }
    }

    private void startPheromones() {

        this.isPheromoneSince = 0;
        this.putPheromones();
    }
}
