package jc.info925.tp1.view;

import javax.swing.*;
import java.awt.*;

/**
 * Created by celinederoland on 2/19/16.
 */
public class PheromonesPanel extends ContentPanel {

    public PheromonesPanel(Color color) {
        super(color);
        this.setBorder(BorderFactory.createEmptyBorder());
    }
}
