package jc.info925.tp1.view;

import jc.info925.tp1.Constant;
import jc.info925.tp1.model.Content;
import jc.info925.tp1.model.Empty;
import jc.info925.tp1.model.Environment;

import javax.swing.*;
import java.awt.*;

/**
 * Created by celinederoland on 2/3/16.
 */
public class Main extends JFrame {
    private Environment environment;

    public Main(Environment environment) {
        this.environment = environment;

        GridLayout matrice = new GridLayout(Constant.HEIGHT, Constant.WIDTH);
        this.getContentPane().setLayout(matrice);

        putContentsOnGrid();

        this.setSize(new Dimension(Constant.WIDTH * 10, Constant.HEIGHT * 10));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    protected void putContentsOnGrid() {
        for(int i = 0; i < Constant.HEIGHT; i++) {
            for(int j = 0; j < Constant.WIDTH; j++) {
                Content content = this.environment.getContent(i,j);
                Color c = content.getColor();
                if(content.isEmpty()) {
                    this.getContentPane().add(new ContentPanel(c, ((Empty) content).getNbPheromonesExplorator(), ((Empty) content).getNbPheromonesEscavator()));
                } else {
                    this.getContentPane().add(new ContentPanel(c));
                }
            }
        }
    }

    public void updateFrame() {

        this.getContentPane().removeAll();

        putContentsOnGrid();

        this.revalidate();
    }
}
