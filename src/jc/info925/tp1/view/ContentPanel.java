package jc.info925.tp1.view;

import jc.info925.tp1.Constant;

import javax.swing.*;
import java.awt.*;

public class ContentPanel extends JPanel {

    public ContentPanel(Color color, int nbPheromonesExplorator, int nbPheromonesExcavator) {

        this(color);
        if (nbPheromonesExcavator + nbPheromonesExplorator > 0) {

            int l = (Constant.NB_PHEROMONE_PER_TURN_EXPLORATOR + Constant.NB_PHEROMONE_PER_TURN_ESCAVATOR) / 10;
            int L = (Constant.NB_TURNS_PHEROMONES_EXPLORATOR + Constant.NB_TURNS_PHEROMONES_ESCAVATOR) / 10;
            GridLayout matrice = new GridLayout(l, L);
            this.setLayout(matrice);
            for (int i = 0; i < (nbPheromonesExplorator/10) && i < l * L; i++) {
                this.add(new PheromonesPanel(Color.BLUE));
            }
            for (int i = (nbPheromonesExplorator/10); i < (nbPheromonesExcavator/10) && i < l * L; i++) {
                this.add(new PheromonesPanel(Color.RED));
            }
            for (int i = (nbPheromonesExplorator/10) + (nbPheromonesExcavator/10); i < l * L; i++) {
                this.add(new PheromonesPanel(Color.WHITE));
            }
        }
    }

    public ContentPanel(Color color) {

        super();
        this.setBackground(color);
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

    }
}
