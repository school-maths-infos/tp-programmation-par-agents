package jc.info925.tp1;

/**
 * Created by celinederoland on 2/3/16.
 */
public class Constant {

    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;
    public static final int NB_EXPLORATOR = 1;
    public static final int NB_EXCAVATOR = 5;
    public static final int NB_TRANSPORTER = 7;
    public static final double PROBA_ORE = 0.05;
    public static final double PROBA_OBSTACLE = 0.05;
    public static final int NB_TURNS_PHEROMONES_EXPLORATOR = 10;
    public static final int NB_PHEROMONE_PER_TURN_EXPLORATOR = 15;
    public static final int NB_TURNS_PHEROMONES_ESCAVATOR = 30;
    public static final int NB_PHEROMONE_PER_TURN_ESCAVATOR = 35;
}
